module.exports = {
  content: ["./index.html", "images", "./src/**/*.{vue,js,ts,jsx,tsx}", "./node_modules/flowbite/**/*.js"],
  theme: {
    colors: {
      primario: "#024e73",
      back: {
        claro: "#ffffff",
        oscuro: "#024e73",
      },
      front: {
        claro: "#FFFFFF",
        oscuro: "#0e3c4b",
      },
      accent: "#5ea969",
      base: "#545760",
      exito: "#5ea969",
      error: "#ea4949",
      info: "#5ea969",
      aviso: "#ea4949",

      blanco: "#FFFFFF",
    },
    extend: {},
  },
  plugins: [require('flowbite/plugin')],
};